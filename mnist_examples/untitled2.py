# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 16:05:04 2017

@author: bloeffel
"""

import numpy as np
import tflearn

from tflearn.datasets import titanic
titanic.download_dataset('titanic_dataset.csv')

from tflearn.data_utils import load_csv
data, labels = load_csv('titanic_dataset.csv', target_column=0, columns_to_ignore=[2,7], categorical_labels=True, n_classes=2)

def handleSex(data):
    for person in data:
        person[1] = 1 if person[1] == 'male' else 0
    return np.array(data, dtype=np.float32)
data = handleSex(data)

# init
tflearn.init_graph(num_cores=8, gpu_memory_fraction=0.5)

# Build Neural Network
net = tflearn.input_data(shape=[None, 6])
net = tflearn.fully_connected(net, 32, activation='relu', name="Layer1")
net = tflearn.fully_connected(net, 32, activation='relu', name="Layer2")
# net = tflearn.dropout(net, 0.8, name="Dropout")
net = tflearn.fully_connected(net, 2, activation='softmax', name="Layer3")
net = tflearn.regression(net, optimizer='adam', learning_rate=0.0005, loss='categorical_crossentropy')

model = tflearn.DNN(net, tensorboard_verbose=3, tensorboard_dir="logs",)

# Training & Save weights
model.fit(data, labels, n_epoch=8, batch_size=16, show_metric=True)
model.save('titanic.model')